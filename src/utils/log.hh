#pragma once

#define CONFIG_LOGLEVEL LogLevel::Trace
#include <iostream>

/**
 * Synchron Logger template class
 *
 * Kaeryv
 */

namespace Log {
enum class LogLevel { Error, Warn, Info, Trace };
constexpr LogLevel Level = CONFIG_LOGLEVEL;

void Print();

template <typename T, typename... P> void Print(const T &a, P... args) {
  std::cout << a<<"\t";
  Print(args...);
}

template <typename T, typename... P> void Info(const T &a, P... args) {
  if   (Level >= LogLevel::Info) {
    std::cout << " :: [INFO] ";
    Print(a, args...);
  }
}
template <typename T, typename... P> void Error(const T &a, P... args) {
  if   (Level >= LogLevel::Error) {
    // TODO: redirect to std::cerr
    std::cout << " :: [ERROR] ";
    Print(a, args...);
  }
}

template <typename T, typename... P> void Warn(const T &a, P... args) {
  if   (Level >= LogLevel::Warn) {
    std::cout << " :: [WARNING] ";
    Print(a, args...);
  }
}

template <typename T, typename... P> void Trace(const T &a, P... args) {
  if   (Level >= LogLevel::Trace) {
    std::cout << " :: [TRACE] ";
    Print(a, args...);
  }
}
}; // namespace Log