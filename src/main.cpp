#include "header.hpp"
#include <fstream>
#include "io/json.hpp"
#include "utils/log.hh"
#include <memory>

int main ()
{
    std::ifstream i("config.json");
    json configuration;
    i >> configuration;
    i.close();
    Log::Info(configuration["test"], "test", "plop");
    auto crom = std::make_shared<Barbar>("Crom",100,10);
    auto kevin = std::make_shared<Character>("Kevin",10);

    return 0;
}