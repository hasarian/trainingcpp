#pragma once
#include <string>
#include <vector>


struct Item{
    uint32_t id;
    std::string name;
    float weight;
};

class Character
{
    protected:
        std::string _name;
        int _hp;
        std::vector<Item> inventory;


    public:

        Character(std::string name,int _hp);

        std::string& name() { return _name; }
        int& hp(){return _hp;}


        ~Character()= default;



};
