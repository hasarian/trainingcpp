
MINGWWORKS= -DWIN32 -DNDEBUG -D_WINDOWS -D_MBCS -Wall -O2 -std=c++11 -m32 -static -static-libgcc -static-libstdc++
CXXFLAGS = -std=c++17 ${MINGWWORKS} 

.PHONY: clean


all: ./main.exe

./main.exe: ./build/main.o ./build/character.o ./build/barbar.o ./build/log.o
	g++ ./build/main.o ./build/character.o ./build/barbar.o ./build/log.o -o ./main.exe ${CXXFLAGS}

./build/main.o: ./src/main.cpp ./src/header.hpp 
	g++ -c ./src/main.cpp -o ./build/main.o ${CXXFLAGS}

./build/barbar.o: ./src/Character/barbar/barbar.cpp ./src/Character/barbar/barbar.hpp ./src/Character/character.hpp
	g++ -c ./src/Character/barbar/barbar.cpp -o ./build/barbar.o ${CXXFLAGS}

./build/character.o: ./src/Character/character.cpp ./src/Character/character.hpp
	g++ -c ./src/Character/character.cpp -o ./build/character.o ${CXXFLAGS}

./build/log.o: ./src/utils/log.cc
	g++ -c ./src/utils/log.cc -o ./build/log.o ${CXXFLAGS}

clean: 
	rm -rf ./build/*.o
	rm -rf ./*.exe